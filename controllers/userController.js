const models = require("../models");
const logs = require("./log").logs;
const User = models.user;
const jwt = require("jsonwebtoken");
const jwtConfig = require("../config/jwtConfig");
const availableUserRoles = require("../config/userRolesConfig");

exports.addUser = async function(req, res) {
  let body = req.body;
  const { pass , n_pass , name} = req.body;
  if ((!pass) || (!n_pass) || (!name)){
    return res.status(401).send({
      code : 0,
      mess : logs.nofill
    });
  }
  if (pass !== n_pass) {
    return res.status(401).send({
      code : 0,
      mess : logs.passxpass
    });
  }
  if (pass.length < 4 || pass.length > 20) {
    return res.status(401).send({
      code : 0,
      mess : logs.invalidPass
    });
  }
  let user = await User.create({
    name: name,
    password: pass,
    role: '0'
  }).then(function(){
    exports.authenticate(req, res);
  }).catch(function(e){
    return res.status(401).send({
      code : -1,
      mess : e//logs.common
    });
  });  
};

exports.authenticate = async function authenticate(req, res, next) {
  const { pass , name} = req.body;
  if (!pass || !name) {
    return res.status(401).send({
      code : 0,
      mess : logs.failLogin
    });
  }
  try {
    const dbUser = await User.findOne({ where: { name } });
    let correctCredentials = (pass == dbUser.password);
    if (correctCredentials) {
      const token = jwt.sign(
        {
          exp: Math.floor(Date.now() / 1000) + 60 * 60,
          id: dbUser.id,
          name: dbUser.name,
          role: dbUser.role
        },
        jwtConfig.secretOrKey
      );
      return res.send({
        code : 100,
        data: {
          user : {
            name: dbUser.name,
            phone: dbUser.name,
            email : dbUser.email,
            avatar : dbUser.avatar
          },
          token : token
        }
      });
    } else {
      return res.status(401).send({
        code : 0,
        mess : logs.failLogin
      });
    }
  } catch (e) {
    return res.status(401).send({
      code : 0,
      mess : logs.failLogin
    });
  }
};

exports.isAdmin = async function(userId) {
  return await User.findOne({
    where: {
      id: userId,
      role: "administrator"
    }
  });
};

exports.getUser = async function(req, res) {
  const userId = req.user.id;
  let user = await User.findOne({
    attributes: ["id", "name", "phone", "email", "avatar", "role"]
  },{
    where: {
      id : userId
    }
  });
  if (!user) {
    res.status(400).send({
      code : -1,
      mess : ''
    });
  }
  else {
    res.status(200).send({
      code : 100,
      mess : 'Lấy thông tin user thành công',
      data : user
    });
  }
};

exports.getUsers = async function(req, res) {
  
};

exports.deleteUser = async function(req, res) {
  try {
    let isAdmin = await exports.isAdmin(req.user.id);
    let userId = req.params.id;

    if (!isAdmin) {
      return res.status(401).send("Unauthorized");
    }

    let user = await User.findOne({
      where: {
        id: userId
      }
    });

    if (!user) {
      throw new Error("User not found");
    }

    if (user.role === "administrator") {
      throw new Error("Unable to delete administrator account");
    }

    User.destroy({
      where: {
        id: userId
      }
    });

    return res.send("user deleted");
  } catch (e) {
    res.status(400).send({ error: e.message });
  }
};

exports.updateUser = async function(req, res) {
  const userId = req.user.id;
  const { phone , email } = req.body;
  if ((!phone) || (!email)){
    res.status(200).send({
      code : 0,
      mess : 'Dữ liệu yêu cầu không được để trống'
    });
  }
  let user = await User.update({ phone: phone, email : email }, {
    where: {
      id : userId
    }
  });
  if (!user) {
    res.status(400).send({
      code : -1,
      mess : ''
    });
  } else {
    res.status(200).send({
      code : 100,
      mess : 'Update thông tin thành công'
    });
  }
};


exports.updateAvatar = async function(req, res) {
  const userId = req.user.id;
  console.log("user ID: "+userId);
  const _file = '/public/uploads/avatar/'+req.file.filename;
  console.log("avatar : "+_file);
  let user = await User.update({ avatar: _file }, {
    where: {
      id : userId
    }
  });
  if (!user) {
    res.status(400).send({
      code : -1,
      mess : ''
    });
  }
  else {
    res.status(200).send({
      code : 100,
      mess : 'Ảnh đại diện đã được cập nhật'
    });
  }
};