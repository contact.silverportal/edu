/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_class', {
    'UID': {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      comment: "null"
    },
    'CID': {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      comment: "null"
    }
  }, {
    tableName: 'user_class'
  });
};
