/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user', {
    'id': {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      primaryKey: true,
      comment: "null",
      autoIncrement: true
    },
    'name': {
      type: DataTypes.STRING(50),
      allowNull: false,
      comment: "null"
    },
    'password': {
      type: DataTypes.STRING(50),
      allowNull: false,
      comment: "null"
    },
    'phone': {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: "null"
    },
    'email': {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: "null"
    },
    'avatar': {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: "null"
    },
    'role': {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0',
      comment: "null"
    },
    'createdAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    },
    'updatedAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    }
  }, {
    tableName: 'user'
  });
};
